const express = require("express");
const path = require("path");
const fs = require("fs");
const cors = require("cors");
const { features } = require("process");

const app = express();

//captura a requisição e se for uma req do tipo 
app.use(express.urlencoded({ extended: false }));
app.use(express.static(path.join(__dirname, "public")));
app.use(express.json());
app.use(cors());

const porta = 3001

app.listen(porta, function () {
    console.log(`Servidor rodando na porta ${porta}`);
})


//localhost:3001/fotospadrao
app.get("/fotospadrao", async function (req, resp) {

    let vetorCandidatos = [];
    vetorCandidatos = await readFileConfigDefault();
    resp.send(vetorCandidatos);

});

//localhost:3001/cargainicial
app.get("/cargainicial", async function (req, resp) {

    let matrizCandidatos = [];
    matrizCandidatos = await readFileConfig();
    resp.send(matrizCandidatos);

});

//localhost:3001/apuracao
app.get("/apuracao", async function (req, resp) {

    let matrizCandidatos = [];
    matrizCandidatos = await readFileConfig();

    let votos = [];
    votos = await readFileVotacao();

    let resultadosEleicao = [];

    resultadosEleicao = await inicializarMatrizResultado(matrizCandidatos);

    resultadosEleicao = obterSomatorioVotos(resultadosEleicao, votos);

    ordenarCandidatosMaisVotados(resultadosEleicao);

    resp.send(resultadosEleicao);
});

/**
 * @description Cria uma matriz com as informações dos candidatos registrados (incluindo as opções de "nulo" e "branco") e inicializa todos com "0" votos.
 * @param {*} candidatos Este parâmetro deve estar carregado com a matriz de candidatos registrados no sistema.
 * @returns Retorna uma matriz inicializada com as informações dos candidatos, votos nulos e brancos começando com "0" votos.
 */
async function inicializarMatrizResultado(candidatos) {
    let resultadosEleicao = [];

    let fotosPadrao = [];

    fotosPadrao = await readFileConfigDefault();

    let votosNulo = ["NULO", "-----", fotosPadrao[0], 0];
    let votosBranco = ["BRANCO", "-----", fotosPadrao[1], 0];

    resultadosEleicao.push(votosNulo);
    resultadosEleicao.push(votosBranco);

    for (let i = 0; i < candidatos.length; i++) {
        let informacoesCand = [];

        //Número do candidato
        informacoesCand.push(candidatos[i][1]);
        //Nome do candidato
        informacoesCand.push(candidatos[i][2]);
        //URL da foto do candidato
        informacoesCand.push(candidatos[i][3]);
        // Quantidade de votos
        informacoesCand.push(0);

        resultadosEleicao.push(informacoesCand);

    }

    return resultadosEleicao;
}

/**
 * @description Faz a leitura do arquivo "configDefault.csv" para obter as fotos referentes ao voto "nulo" e "branco".
 * @returns Retorna uma _promisse_ contendo um vetor prenchido com fotos padrões da urna ("voto branco", "voto nulo" e "candidato não identificado").
 */
async function readFileConfigDefault() {
    return new Promise((resolve, reject) => {
        fs.readFile('configDefault.csv', "utf8", function (err, data) {
            if (err) {
                console.log("Erro ao ler arquivo: " + err);
                resolve(err);
            } else {
                let fotosPadrao = [];
                fotosPadrao = data.split("\r\n");

                resolve(fotosPadrao);
            }
        });
    });
}

/**
 * @description Faz a leitura do arquivo "config.csv" para obter os dados (tipo da votação, número, nome e url da foto) referentes aos candidatos registrados.
 * @returns Retorna uma _promisse_ contendo uma matriz prenchida com as informações dos candidatos registrados no sistema.
 */
async function readFileConfig() {
    return new Promise((resolve, reject) => {
        fs.readFile('config.csv', "utf8", function (err, data) {
            if (err) {
                console.log("Erro ao ler arquivo: " + err);
                resolve(err);
            } else {
                console.log(data);

                let candidato = data.split("\r\n")
                console.log(candidato);

                let candidatos = []

                for (let i = 0; i < candidato.length; i++) {
                    const element = candidato[i];
                    candidatos.push(element.split(","))
                }

                resolve(candidatos);
            }
        });
    });
}

/**
 * @description Faz a leitura do arquivo "votacao.csv" par obter a apuração da urna eletrônica.
 * @returns Retorna uma _promisse_ contendo uma matriz prenchida com as informações dos votos registrados no sistema (podendo conter ou não o RG de quem votou).
 */
async function readFileVotacao() {
    return new Promise((resolve, reject) => {
        fs.readFile('votacao.csv', "utf8", function (err, data) {
            if (err) {
                console.log("Erro ao ler arquivo: " + err);
                resolve(err);
            } else {
                let votos = []

                let infCand = data.split("\n")

                for (let i = 0; i < infCand.length; i++) {
                    const element = infCand[i];
                    votos.push(element.split(","))
                }

                resolve(votos);
            }
        });
    });
}

/**
 * @description Esta função é responsabilizada por realizar o somatório de votos da urna eletrônica.
 * @param {*} resultadosEleicao Este parâmetro deve conter a matriz dos candidatos registrados no sistema, a opção de "voto nulo" e "voto em branco" inicializados com "0" votos.
 * @param {*} votos Este parâmetro deve conter a matriz de votos registrados no sistema.
 * @returns Retona a matriz da apuração da urna eletrônica com os votos devidamente contabilizados.
 */
function obterSomatorioVotos(resultadosEleicao, votos) {

    let rgContabilizados = [];

    for (let i = 0; i < votos.length; i++) {

        let rgAtual = votos[i][0];

        // Verificação dos votos do tipo "não anônimo" (com RG preenchido).
        if (rgAtual != undefined && rgAtual != "") {
            if (!rgContabilizados.includes(rgAtual)) {
                rgContabilizados.push(rgAtual);
            } else {
                continue;
            }
        }

        let numVoto = votos[i][1];
        switch (numVoto) {
            case "N-U-L-O":
                resultadosEleicao[0][3]++; // Incremento dos votos nulos.
                break;
            case "B-R-A-N-C-O":
                resultadosEleicao[1][3]++; // Incremento dos votos em branco.
                break;
            default:
                for (let j = 0; j < resultadosEleicao.length; j++) {
                    let numCand = resultadosEleicao[j][0];
                    if (numCand == numVoto) {
                        resultadosEleicao[j][3]++; // Incremento dos votos do candidato votado.
                        break;
                    }
                }
        }
    }
    return resultadosEleicao;
}

/**
 * @description Ordena os candidatos mais votados de forma decrescente. Caso haja candidatos com a mesma quantidade de votos, os mesmos serão ordenados em ordem alfabética pelo nome.
 * @param {*} resultadosEleicao Este parâmetro deve conter a matriz da apuração da urna com os votos já contabilizados.
 */
function ordenarCandidatosMaisVotados(resultadosEleicao) {
    resultadosEleicao.sort(function (x, y) {
        //Ordena pela quantidade de votos (maior -> menor)
        if (x[3] > y[3]) {
            return -1
        } else if (x[3] < y[3]) {
            return 1
        } else {
            //Ordena em ordem alfabética (A -> Z)
            if (x[1] < y[1]) {
                return -1;
            } else if (x[1] > y[1]) {
                return 1;
            } else {
                return 0;
            }
        }
    });
}

// Captura um novo voto e grava no arquivo "votacao.csv" (http://localhost:3001/voto).
app.post("/voto", function (req, resp) {

    let timestamp = new Date().getTime();

    console.log("RG capturado: " + req.body.rg);
    console.log("Número do Voto capturado: " + req.body.voto);

    let voto = req.body.rg + "," + req.body.voto + "," + timestamp + "\n";

    fs.appendFile("votacao.csv", voto, function (err) {
        if (err) {
            console.log("Erro ao gravar o voto: " + err);
            resp.json(
                {
                    "status": 500,
                    "mensagem": err
                }
            );
        } else {
            console.log("Salvou o voto:\t[" + voto + "]")
            resp.json(
                {
                    "status": 200,
                    "mensagem": "Voto salvo com sucesso!"
                });
        }
    })
})
